import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ntt_rates/widgets/list_view.dart';
import 'package:provider/provider.dart';

import '../Mocks/mock_currency_notifier.dart';

void main() {
  Widget widgetToPump() {
    return MaterialApp(
      home: Scaffold(
        body: ChangeNotifierProvider(
          create: (_) => MockCurrencyNotifier(),
          builder: (context, _) {
            context.read<MockCurrencyNotifier>().updateCurrencies('gbp');
            return Consumer<MockCurrencyNotifier>(
              builder: (context, notifier, child) {
                return RatesListView(
                  currencies: notifier.filterCurrencies,
                  onRefresh: () {
                    notifier.updateCurrencies('gyd');
                  },
                  onNavigationComplete: () {
                    notifier.refreshCurrencies();
                  },
                );
              },
            );
          },
        ),
      ),
    );
  }

  group('List View Tests', () {
    testWidgets(
      'When loaded the RatesListView should be displayed.',
      (WidgetTester tester) async {
        await tester.pumpWidget(widgetToPump());
        expect(find.byType(RatesListView), findsOneWidget);
      },
    );

    testWidgets(
      'List should have two(2) items.',
      (WidgetTester tester) async {
        await tester.pumpWidget(widgetToPump());
        await tester.pumpAndSettle();
        expect(find.byType(ListTile), findsNWidgets(2),
            reason: 'Found more or less than 2 items nin list');
      },
    );

    testWidgets(
      'Verify that the title of first list item is GYD 282.82.',
      (WidgetTester tester) async {
        await tester.pumpWidget(widgetToPump());
        final listTile = tester.widget(find.byType(ListTile).first) as ListTile;
        final title = listTile.title as Text;
        expect(title.data, equals('GYD 282.82'));
      },
    );

    testWidgets(
      'Verify that the subtitle of first list item is GUYANA.',
      (WidgetTester tester) async {
        await tester.pumpWidget(widgetToPump());
        final listTile = tester.widget(find.byType(ListTile).first) as ListTile;
        final subtitle = listTile.subtitle as Text;
        expect(subtitle.data, equals('GUYANA'));
      },
    );
  });
}
