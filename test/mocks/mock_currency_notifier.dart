import 'package:mockito/mockito.dart';
import 'package:ntt_rates/models/currency.dart';
import 'package:ntt_rates/pages/currency_notifier.dart';

class MockCurrencyNotifier extends Mock implements CurrencyNotifier {
  List<Currency> _currencies = [];
  List<Currency> _filterCurrencies = [];

  @override
  List<Currency> get filterCurrencies => _filterCurrencies;

  @override
  Future<void> updateCurrencies(String currencyCode) async {
    if (currencyCode.toLowerCase() == 'gyd') {
      _currencies = [
        Currency(
          country: 'Trinidad & Tobago',
          code: 'ttd',
          amount: 9.19,
        )
      ];
    } else {
      _currencies = [
        Currency(
          country: 'Guyana',
          code: 'gyd',
          amount: 282.82,
        ),
        Currency(
          country: 'Trinidad & Tobago',
          code: 'ttd',
          amount: 9.19,
        )
      ];
    }
    _filterCurrencies = _currencies;
    notifyListeners();
  }

  @override
  Future<void> refreshCurrencies() async {
    _filterCurrencies = _currencies;
    notifyListeners();
  }
}
