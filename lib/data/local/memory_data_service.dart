import '../../models/currency.dart';
import 'cache_data_service.dart';

class LocalMemoryConversionRatesService implements LocalConversionRatesService {
  List<Currency> rates = [];

  @override
  late String selectedCurrencyCode;

  @override
  Future<List<Currency>> getRates() async {
    return Future.value(rates);
  }

  @override
  Future<void> setRates(List<Currency> newRates) async {
    rates = newRates;
  }
}
