import '../../models/currency.dart';
import '../data_service.dart';

abstract class LocalConversionRatesService implements ConversionRatesService {
  Future<void> setRates(List<Currency> newRates);
}
