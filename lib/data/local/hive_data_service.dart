import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

import '../../models/currency.dart';
import 'cache_data_service.dart';

class HiveConversionRatesService implements LocalConversionRatesService {
  HiveConversionRatesService._privateConstructor();
  static final HiveConversionRatesService instance =
      HiveConversionRatesService._privateConstructor();
  static const _boxName = 'ntt_rates';
  static const _key = 'rates';

  @override
  Future<List<Currency>> getRates() async {
    var box = await _openHiveBox();
    final List<Map<String, dynamic>> rates = await box.get(_key);
    final result = rates.map((e) => e.toConversionRate()).toList();
    return Future.value(result);
  }

  @override
  Future<void> setRates(List<Currency> newRates) async {
    List<Map<String, dynamic>> rates = newRates.map((e) => e.toMap()).toList();
    var box = await _openHiveBox();
    await box.put(_key, rates);
  }

  Future<Box> _openHiveBox() async {
    if (!Hive.isBoxOpen(_boxName)) {
      Hive.init((await getApplicationDocumentsDirectory()).path);
    }
    return Hive.openBox(_boxName);
  }

  @override
  late String selectedCurrencyCode;
}

extension _GbpConversionRateMapConverter on Currency {
  Map<String, dynamic> toMap() {
    return {
      'currency': country,
      'currencyCode': code,
      'amount': amount,
    };
  }
}

extension _GbpConversionRateConverter on Map<String, dynamic> {
  Currency toConversionRate() {
    return Currency(
      country: this['currency'],
      code: this['currencyCode'],
      amount: this['amount'],
    );
  }
}
