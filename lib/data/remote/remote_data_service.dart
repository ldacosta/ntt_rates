import 'dart:convert';

import 'package:http/http.dart';

import '../../models/currency.dart';
import '../data_service.dart';

class RemoteConversionRatesService implements ConversionRatesService {
  final Client _apiClient = Client();

  @override
  late String selectedCurrencyCode;

  RemoteConversionRatesService();

  @override
  Future<List<Currency>> getRates() async {
    final tempRates = await _getLatestConversionRates();
    final currencies = await _getLatestCurrencies();
    List<Currency> rates = [];
    tempRates.forEach((key, value) => {
          rates.add(
            Currency(
              country: currencies[key],
              code: key,
              amount: value.toDouble(),
            ),
          )
        });
    return Future.value(rates);
  }

  Future<Map<String, dynamic>> _getLatestConversionRates() async {
    final url = Uri.https('cdn.jsdelivr.net',
        '/gh/fawazahmed0/currency-api@1/latest/currencies/$selectedCurrencyCode.json');
    final response = await _apiClient.get(
      url,
      headers: {"Content-Type": "application/json; charset=utf-8"},
    ).timeout(
      const Duration(seconds: 10),
    );

    if (response.statusCode != 200) {
      return {};
    }
    return jsonDecode(response.body)[selectedCurrencyCode] ?? {};
  }

  Future<Map<String, dynamic>> _getLatestCurrencies() async {
    final url = Uri.https('cdn.jsdelivr.net',
        '/gh/fawazahmed0/currency-api@1/latest/currencies.json');
    final response = await _apiClient.get(
      url,
      headers: {"Content-Type": "application/json; charset=utf-8"},
    ).timeout(
      const Duration(seconds: 10),
    );

    if (response.statusCode != 200) {
      return {};
    }
    return jsonDecode(response.body) ?? {};
  }
}
