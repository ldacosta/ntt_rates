import '../models/currency.dart';
import 'local/cache_data_service.dart';

abstract class ConversionRatesService {
  late String selectedCurrencyCode;
  Future<List<Currency>> getRates() async {
    return [];
  }
}

class ConversionRatesLocalFirstServiceImpl implements ConversionRatesService {
  final LocalConversionRatesService local;
  final ConversionRatesService remote;
  @override
  late String selectedCurrencyCode;

  ConversionRatesLocalFirstServiceImpl({
    required this.local,
    required this.remote,
  });

  @override
  Future<List<Currency>> getRates() async {
    try {
      final oldData = await local.getRates();
      if (oldData.isEmpty) {
        final freshData = await remote.getRates();
        if (freshData.isNotEmpty) {
          local.setRates(freshData);
        }
        return freshData;
      }
      return oldData;
    } catch (_) {
      return [];
    }
  }
}

class ConversionRatesRemoteFirstServiceImpl implements ConversionRatesService {
  final ConversionRatesService remote;
  final LocalConversionRatesService local;

  @override
  late String selectedCurrencyCode;

  ConversionRatesRemoteFirstServiceImpl({
    required this.remote,
    required this.local,
  });

  @override
  Future<List<Currency>> getRates() async {
    try {
      final freshData = await remote.getRates();
      if (freshData.isNotEmpty) {
        local.setRates(freshData);
        return freshData;
      }
      return local.getRates();
    } catch (_) {
      return local.getRates();
    }
  }
}
