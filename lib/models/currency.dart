class Currency {
  final String country;
  final String code;
  final double amount;

  Currency({
    required this.country,
    required this.code,
    required this.amount,
  });
}
