import 'package:flutter/foundation.dart';
import 'package:ntt_rates/data/local/cache_data_service.dart';

import '../data/data_service.dart';
import '../models/currency.dart';

class CurrencyNotifier with ChangeNotifier {
  final ConversionRatesService remoteRateService;
  final LocalConversionRatesService localRateService;

  CurrencyNotifier({
    required this.remoteRateService,
    required this.localRateService,
  });
  List<String> currencyCodes = [];

  bool _shouldRefresh = true;
  List<Currency> _currencies = [];
  List<Currency> _filterCurrencies = [];
  List<Currency> get filterCurrencies => _filterCurrencies;

  ConversionRatesService get _dataService {
    remoteRateService.selectedCurrencyCode = currencyCodes.last;
    localRateService.selectedCurrencyCode = currencyCodes.last;

    if (_shouldRefresh) {
      return ConversionRatesRemoteFirstServiceImpl(
        remote: remoteRateService,
        local: localRateService,
      );
    } else {
      return ConversionRatesLocalFirstServiceImpl(
        local: localRateService,
        remote: remoteRateService,
      );
    }
  }

  Future<void> updateCurrencies(String currencyCode) async {
    _shouldRefresh = false;
    _setCurrencies(await _dataService.getRates());
    notifyListeners();
  }

  Future<void> refreshCurrencies() async {
    _shouldRefresh = true;
    _setCurrencies(await _dataService.getRates());
    notifyListeners();
  }

  Future<void> filterCurrency(term) async {
    _filterCurrencies = _currencies
        .where(
          (e) => (e.country
                  .toLowerCase()
                  .toString()
                  .contains(term.toLowerCase()) ||
              e.code.toLowerCase().toString().contains(term.toLowerCase())),
        )
        .toList();
    notifyListeners();
  }

  void _setCurrencies(List<Currency> currencies) {
    currencies.removeWhere((element) =>
        element.code.toLowerCase().trim() ==
        currencyCodes.last.toLowerCase().trim());
    _currencies = currencies;
    _filterCurrencies = currencies;
  }
}
