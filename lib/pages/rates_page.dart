import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

import '../pages/currency_notifier.dart';
import '../widgets/list_view.dart';
import '../widgets/search_bar.dart';

class RatesPage extends StatefulWidget {
  final String title;
  final String currencyCode;

  RatesPage({
    Key? key,
    required this.title,
    required this.currencyCode,
  }) : super(key: key);

  final controllerSearch = TextEditingController();
  final keySearchBar = GlobalKey<SearchBarState>();

  @override
  State<RatesPage> createState() => _RatesPageState();
}

class _RatesPageState extends State<RatesPage> {
  Timer? _debouncer;

  @override
  void initState() {
    super.initState();
    final currencyNotifier = context.read<CurrencyNotifier>();
    currencyNotifier.currencyCodes.add(widget.currencyCode);
    currencyNotifier.refreshCurrencies();
  }

  @override
  void dispose() {
    _debouncer?.cancel();
    super.dispose();
  }

  void debounce(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 500),
  }) {
    if (_debouncer != null) {
      _debouncer!.cancel();
    }
    _debouncer = Timer(duration, callback);
  }

  Widget? _backButton(BuildContext context) {
    if (Navigator.of(context).canPop()) {
      return IconButton(
        icon: const Icon(
          Icons.chevron_left,
          color: Colors.grey,
          size: 38,
        ),
        onPressed: () {
          CurrencyNotifier notifier = context.read<CurrencyNotifier>();
          notifier.currencyCodes.removeLast();
          context.read<CurrencyNotifier>().refreshCurrencies();
          Navigator.of(context).pop();
        },
      );
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: _backButton(context),
        centerTitle: false,
        title: Text(
          '${widget.title} - ${widget.currencyCode.toUpperCase()}',
          style: const TextStyle(
            color: Colors.grey,
          ),
        ),
        backgroundColor: Colors.black87,
        titleTextStyle: const TextStyle(
          color: Colors.white,
          fontSize: 22,
        ),
        actionsIconTheme: const IconThemeData(
          color: Colors.white,
        ),
        iconTheme: const IconThemeData(
          color: Colors.white, //change your color here
        ),
      ),
      body: Consumer<CurrencyNotifier>(
        builder: (context, notifier, child) {
          return Column(
            children: [
              _buildSearchWidget(context),
              Expanded(
                child: RatesListView(
                  currencies: notifier.filterCurrencies,
                  onRefresh: () {
                    widget.controllerSearch.clear();
                    notifier.refreshCurrencies();
                  },
                  onNavigationComplete: () {
                    widget.controllerSearch.clear();
                    notifier.refreshCurrencies();
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      key: UniqueKey(),
      child: const Text(
        'Please wait, loading rates...',
      ),
    );
  }

  Widget _buildSearchWidget(BuildContext context) {
    return SearchBar(
      key: widget.keySearchBar,
      controller: widget.controllerSearch,
      hintText: 'Currency name or code',
      onSearch: (searchTerm) {
        context.read<CurrencyNotifier>().filterCurrency(searchTerm);
      },
      onClear: () {
        context.read<CurrencyNotifier>().refreshCurrencies();
      },
    );
  }
}
