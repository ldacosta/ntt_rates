import 'package:auto_route/annotations.dart';

import '../pages/rates_page.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    AutoRoute(page: RatesPage, initial: true),
  ],
)
class $AppRouter {}
