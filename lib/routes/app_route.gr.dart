// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i2;
import 'package:flutter/material.dart' as _i3;

import '../pages/rates_page.dart' as _i1;

class AppRouter extends _i2.RootStackRouter {
  AppRouter([_i3.GlobalKey<_i3.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i2.PageFactory> pagesMap = {
    RatesPageRoute.name: (routeData) {
      final args = routeData.argsAs<RatesPageRouteArgs>();
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i1.RatesPage(
              key: args.key,
              title: args.title,
              currencyCode: args.currencyCode));
    }
  };

  @override
  List<_i2.RouteConfig> get routes =>
      [_i2.RouteConfig(RatesPageRoute.name, path: '/')];
}

/// generated route for
/// [_i1.RatesPage]
class RatesPageRoute extends _i2.PageRouteInfo<RatesPageRouteArgs> {
  RatesPageRoute(
      {_i3.Key? key, required String title, required String currencyCode})
      : super(RatesPageRoute.name,
            path: '/',
            args: RatesPageRouteArgs(
                key: key, title: title, currencyCode: currencyCode));

  static const String name = 'RatesPageRoute';
}

class RatesPageRouteArgs {
  const RatesPageRouteArgs(
      {this.key, required this.title, required this.currencyCode});

  final _i3.Key? key;

  final String title;

  final String currencyCode;

  @override
  String toString() {
    return 'RatesPageRouteArgs{key: $key, title: $title, currencyCode: $currencyCode}';
  }
}
