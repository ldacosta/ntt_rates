import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ntt_rates/pages/currency_notifier.dart';
import 'package:provider/provider.dart';

import '../pages/rates_page.dart';
import 'data/local/hive_data_service.dart';
import 'data/remote/remote_data_service.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => CurrencyNotifier(
        localRateService: HiveConversionRatesService.instance,
        remoteRateService: RemoteConversionRatesService(),
      ),
      child: MaterialApp(
        title: 'NTT Data Rates',
        theme: ThemeData(
          primarySwatch: Colors.grey,
          accentColor: Colors.blue,
        ),
        routes: {
          '/': (context) => RatesPage(
                title: 'NTTData',
                currencyCode: 'gbp',
              ),
        },
        onGenerateRoute: (settings) {
          return CupertinoPageRoute(
            builder: (BuildContext context) {
              return RatesPage(
                title: "NTTData",
                currencyCode: settings.name ?? "/",
              );
            },
          );
        },
      ),
    );
  }
}
