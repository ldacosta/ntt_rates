import 'package:flutter/cupertino.dart';

import '../models/currency.dart';

class CurrencyImage extends StatelessWidget {
  final Currency currency;

  const CurrencyImage({
    Key? key,
    required this.currency,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final imagePath =
        "https://raw.githubusercontent.com/transferwise/currency-flags/master/src/flags/${currency.code}.png";
    final cryptoIconPath =
        "https://cryptoicons.org/api/color/${currency.code}/100";
    final cryptoIconPath2 =
        "https://cryptoicon-api.vercel.app/api/icon/${currency.code}";

    return SizedBox(
      width: 100,
      child: FadeInImage.assetNetwork(
        placeholder: 'assets/images/loading.gif',
        image: imagePath,
        imageErrorBuilder: (context, obj, trace) {
          return FadeInImage.assetNetwork(
            placeholder: 'assets/images/loading.gif',
            image: cryptoIconPath2,
            imageErrorBuilder: (context, obj, trace) {
              return const Placeholder();
            },
          );
        },
      ),
    );
  }
}
