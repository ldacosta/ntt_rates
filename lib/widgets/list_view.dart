import 'dart:async';

import 'package:flutter/material.dart';

import '../models/currency.dart';
import 'currency_image.dart';

class RatesListView extends StatefulWidget {
  final List<Currency> currencies;
  final Function onRefresh;
  final Function onNavigationComplete;

  const RatesListView({
    Key? key,
    required this.currencies,
    required this.onRefresh,
    required this.onNavigationComplete,
  }) : super(key: key);

  @override
  RatesListViewState createState() => RatesListViewState();
}

class RatesListViewState extends State<RatesListView> {
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      key: UniqueKey(),
      backgroundColor: Theme.of(context).backgroundColor,
      color: Theme.of(context).colorScheme.primaryVariant,
      strokeWidth: 4,
      triggerMode: RefreshIndicatorTriggerMode.onEdge,
      onRefresh: _refresh,
      child: ListView.separated(
        key: UniqueKey(),
        itemCount: widget.currencies.length,
        itemBuilder: (context, index) {
          return buildItem(widget.currencies[index]);
        },
        separatorBuilder: (context, index) => Container(
          padding: const EdgeInsets.only(left: 100),
          child: const Divider(
            color: Colors.black,
          ),
        ),
        padding: EdgeInsets.zero,
      ),
    );
  }

  ListTile buildItem(Currency currency) {
    return ListTile(
      key: ObjectKey(currency),
      contentPadding:
          const EdgeInsets.only(left: 0, top: 0, right: 10, bottom: 0),
      leading: CurrencyImage(currency: currency),
      title: Text('${currency.code.toUpperCase()} ${currency.amount}'),
      subtitle: Text(currency.country.toUpperCase()),
      trailing: const Icon(
        Icons.keyboard_arrow_right,
        color: Colors.black,
        size: 38,
      ),
      onTap: () {
        Navigator.pushNamed(
          context,
          currency.code,
          arguments: {
            'rate': currency,
          },
        ).whenComplete(() => widget.onNavigationComplete);
      },
    );
  }

  Future<void> _refresh() async {
    setState(() {
      widget.onRefresh();
    });
  }
}
